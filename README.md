# A workshop to learn some Docker Swarm

##  Summary

```
$ docker stack deploy -c docker-compose.yml workshop

$ docker service ls

$ docker service update workshop_first_service --image vsis/first_service:200

$ docker service update workshop_second_service --image vsis/second_service:200

$ docker service rollback workshop_first_service

$ docker service scale workshop_second_service=1

$ docker service scale workshop_first_service=3

$ docker service logs workshop_first_service

$ docker service logs workshop_second_service

$ docker stack rm workshop
```

## Docs

Docker compose file: https://docs.docker.com/compose/compose-file/

Docker stack commands: https://docs.docker.com/engine/reference/commandline/stack/

Docker service commands: https://docs.docker.com/engine/reference/commandline/service/
