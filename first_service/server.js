'use strict';

const express = require('express');
const os = require("os");
const fs = require('fs')

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
const hostname = os.hostname();
const config_file = '/etc/first'

// App
const app = express();
app.get('/', (req, res) => {
  fs.readFile(config_file, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log('somebody is hitting first service')
    res.type('text/plain')
    res.send('First service is up =) I am: ' + hostname + ' version: 100' + os.EOL + data);
  });
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
